(use-package! evil-colemak-basics
  :after evil
  :config
  (setq evil-colemak-basics-rotate-t-f-j t))

(after! evil (global-evil-colemak-basics-mode))

(setq evil-colemak-basics-char-jump-commands 'evil-snipe)

(setq-default evil-escape-key-sequence "ne"
              evil-escape-unordered-key-sequence t)

(after! evil
  (evil-global-set-key 'normal (kbd ".") 'nil)
  (evil-global-set-key 'normal (kbd "..") 'evil-repeat)
  (evil-global-set-key 'normal (kbd "<left>") 'evil-window-left)
  (evil-global-set-key 'normal (kbd "<down>") 'evil-window-down)
  (evil-global-set-key 'normal (kbd "<up>") 'evil-window-up)
  (evil-global-set-key 'normal (kbd "<right>") 'evil-window-right)
  (evil-global-set-key 'normal (kbd ";") 'evil-ex)
  ;; TODO: add mapping for folding

  (evil-set-leader 'normal (kbd ","))
  (evil-global-set-key 'normal (kbd "<leader>w") 'evil-write)
)



(provide 'jat--evil)
